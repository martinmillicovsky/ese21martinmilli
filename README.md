# ese21martinmilli

## **Proyecto** <br/>
Analizador de impedancia con QCM y EDU-CIIA para análisis de lágrima humana y detección de patologías <br/>
El objetivo del proyecto es utilizar el EDU-CIIA para comunicarse con un poncho/shield. Este dispositivo sería un analizador de impedancia basada en un cristal QCM. <br/>

#### **Diagrama en bloques** <br/>

![Screenshot](images/block diagram.png)

**Shield - Analizador de Impedancia**  

![Screenshot](images/SHIELD.png)

#### **Explicación del proyecto**<br/>

El dispositivo inicia usando como módulo de sensado el cristal "XTAL" QCM de 10MHz con electrodos de oro. El sistema permite una cuantificación en tiempo real de la masa y la viscosidad/densidad (D) de la solución depositada en electrodo de cuarzo. El cambio en la frecuencia de resonancia (f0) es directamente proporcional al de la masa depositada y D es útil cuando la interfaz del cristal características viscoelásticas.<br/>

Para obtener f0 y D, se busca la respuesta en frecuencia del cristal barriendo en frecuencia hasta los 100MHz usando un generador de señales. Para este propósito se usa el _AD9951_. <br/>

Todo comienza por generar la señal base que finalmente se compara con la que genera el QCM. Para ello tenemos el circuito clock con el cristal smd que genera las señales clock input y la orden del Edu-Ciia.<br/>

El AD9551 genera la señal y luego pasa un bloque llamado VCCS (Voltage-Controlled Current Source). Hablamos de un circuito que convierte voltaje a corriente, por eso vemos “Io” luego del bloque. También se le conoce como OTA (amplificador operacional de transconductancia). Este nombre es mas conocido, ya que en el circuito de clock el que realiza esa tarea es el OPA860. Los otros dos integrados cumplen funciones que hemos explicado previamente. Entonces el circuito de clock comprende la generación de la señal y el VCSS. <br/>

La señal se inserta en una etapa de referencia compuesta por dos amplificadores. Una donde Rs, la señal del AD9551, sirve como referencia de ganancia y fase. La restante utiliza la señal del cristal QCM. Ambos amplificadores son los AD8129 vistos previamente. El cristal se maneja con el bloque CCVS (Current-Controlled Voltaje Source). Es una fuente de voltaje controlada por corriente que se implementó con el ADA4817 y una ganancia estable de valor “1”. <br/>

Finalmente, la salida de ambos amplificadores va al detector de ganancia/fase realizada con el _AD8302_. La información será adquirida por el Edu-Ciia para su estudio.<br/>

#### **Resumen del trabajo con EDU-CIIA** <br/>

El EDU-CIIA debe comunicarse con el AD9951 via SPI para que el sintetizador comience a funcionar. Luego todo lo que sigue es una cuestión circuital. Finalmente llegamos al AD8302 y necesitamos adquirir la información de ganancia y fase para poder estudiarla. Estos datos se obtiene usando los pines de lectura de voltaje analógico.<br/>
Esta información debe ser procesada y la idea es poder ver a traves de un monitor serie, en la pc, los valores que obtenemos para interpretrar si son correctos o erróneos para finalmente generar un archivo de texto con los datos y, a partir de esto, plasmarlos en gráficos en excel.<br/>

**Esquema de capas**<br/>

![Screenshot](images/esquema de capas.png)
