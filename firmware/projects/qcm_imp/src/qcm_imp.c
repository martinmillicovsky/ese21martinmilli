/*==================[inclusions]=============================================*/

#include "qcm_imp.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"
#include "spi.h"
/*==================[macros and definitions]=================================*/
 #define tsAD 1000  //Timer de duración de barrido

//Definición y configuraciones de los pines para el AD9951

	 gpioConf_t gp1 = {GPIO_1, OUTPUT, NONE_RES}; //CLKMODESEL-DIRECTO A 1,8V
	 gpioConf_t gp2 = {GPIO_2, OUTPUT, NONE_RES}; //IO_UPDATE
	 gpioConf_t gp3 = {GPIO_3, OUTPUT, NONE_RES}; //CHIP SELECT
	 gpioConf_t gp4 = {GPIO_4, OUTPUT, NONE_RES}; //RESET
	 gpioConf_t gp5 = {GPIO_5, OUTPUT, NONE_RES}; //PWRDWNCTRL


/*==================[internal data definition]===============================*/
//Variables blinks
#define COUNT_DELAY 1000000
#define MICRO_DELAY 5000

/*==================[internal functions declaration]=========================*/

//JUEGO DE LEDS
void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

//JUEGO DE LEDS
void Delay2(void)
{
	uint32_t i;

	for(i=MICRO_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

void blinks()
{
	Led_On(YELLOW_LED);
	Delay();
	Led_Off(YELLOW_LED);
}
void blinks2()
{
	Led_On(GREEN_LED);
	Delay();
	Led_Off(GREEN_LED);
}


/*==================[external data definition]===============================*/

serial_config usbserial = {SERIAL_PORT_PC, 9600, NULL};

analog_input_config ADdb_config = {CH1,AINPUTS_SINGLE_READ,NULL};
analog_input_config ADph_config = {CH2,AINPUTS_SINGLE_READ,NULL};

spiConfig_t sintconf = {SPI_1, MASTER, MODE3, 20000000, SPI_INTERRUPT, NULL};

/*==================[external functions definition]==========================*/
int pot2(int exponent)
{
    int base = 2;
    int resultado = 1;
    for (exponent; exponent > 0; exponent--)
    {
        resultado = resultado * base;
    }
    return resultado;
}

void pins9951()
{
	GPIOInit(gp1);//CLKMODESELECT
	GPIOInit(gp2);//IOUPDATE
	GPIOInit(gp3);//CS
	GPIOInit(gp4);//RESET
	GPIOInit(gp5);//PWRDWNCTRL


	GPIOSetLow(GPIO_1);
	GPIOSetLow(GPIO_2);
	GPIOSetHigh(GPIO_3);
	GPIOSetLow(GPIO_4);
	GPIOSetLow(GPIO_5);
}


void toggleReset()
{
	GPIOSetHigh(GPIO_4);
	Delay2();
	GPIOSetLow(GPIO_4);
}

void toggleIOUP()
{
	GPIOSetHigh(GPIO_2);
	Delay2();
	GPIOSetLow(GPIO_2);
}

void config9951()
{
	 toggleReset();

	 uint8_t cfr1def[5]={0x00, 0x00, 0x00, 0x12, 0x00};
	 uint8_t cfr2def[4]={0x01, 0x00 , 0x02, 0x84};
	 //0x00 pll disabled//0x74 pll=336mhz //0x30 pll = 144mhz //0x84 pll = 384 mhz
	 uint8_t asfdef[3]={0x02, 0x3F, 0xFF};
	 uint8_t arrdef[2]={0x03, 0xFF};
	 uint8_t ftwdef[5]={0x04, 0x35, 0x55, 0x55, 0x55};
	 uint8_t powdef[3]={0x05, 0x00, 0x00};


		GPIOSetLow(GPIO_3);
		SpiWrite(SPI_1, cfr1def, 5);
		GPIOSetHigh(GPIO_3);
		toggleIOUP();


		GPIOSetLow(GPIO_3);
		SpiWrite(SPI_1, cfr2def, 4);
		GPIOSetHigh(GPIO_3);
		toggleIOUP();

		GPIOSetLow(GPIO_3);
		SpiWrite(SPI_1, asfdef, 3);
		GPIOSetHigh(GPIO_3);
		toggleIOUP();

		GPIOSetLow(GPIO_3);
		SpiWrite(SPI_1, arrdef, 2);
		GPIOSetHigh(GPIO_3);
		toggleIOUP();

		GPIOSetLow(GPIO_3);
		SpiWrite(SPI_1, ftwdef, 5);
		GPIOSetHigh(GPIO_3);
		toggleIOUP();

		GPIOSetLow(GPIO_3);
		SpiWrite(SPI_1, powdef, 3);
		GPIOSetHigh(GPIO_3);
		toggleIOUP();


}

void configpll(uint8_t mult)
{


	 uint8_t cfr2def[4]={0x01, 0x00 , 0x02, mult};
	 //0x00 pll disabled//0x74 pll=336mhz //0x30 pll = 144mhz //0x84 pll = 384 mhz

		GPIOSetLow(GPIO_3);
		SpiWrite(SPI_1, cfr2def, 4);
		GPIOSetHigh(GPIO_3);
		toggleIOUP();
}

void read9951()
{

	uint8_t reg0[4]={0x09, 0x0C, 0x12, 0x1F};
	uint8_t rinst[6]={0x80, 0x81, 0x82, 0x83, 0x84, 0x85};
	uint32_t vsiz[6]={4, 3, 2, 1, 4, 2};

	//PASO 1 - LEER EL REGISTRO Y VER 9-12-18-31  EN DECIMAL


	UartSendString(SERIAL_PORT_PC, (uint8_t *) "BUFFER POR DEFECTO : ");
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa(reg0[0],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa(reg0[1],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa(reg0[2],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	UartSendString(SERIAL_PORT_PC, UartItoa(reg0[3],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");


	//PASO 2 - LEER LECT Y VER 0X00
	for(int i=0; i<6; i++)
	{

		GPIOSetLow(GPIO_3);

		SpiWrite(SPI_1, &rinst[i], 1);

		//SpiWrite(SPI_1, 0x00, 4);


		SpiRead(SPI_1, reg0, vsiz[i]);

		GPIOSetHigh(GPIO_3);
		toggleIOUP();


	UartSendString(SERIAL_PORT_PC, (uint8_t *) "BUFFER CON REGISTRO : ");
	UartSendString(SERIAL_PORT_PC, UartItoa(i,10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "-->");
	UartSendString(SERIAL_PORT_PC, UartItoa(reg0[0],10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");

	if(vsiz[i]>1)
	{
		UartSendString(SERIAL_PORT_PC, (uint8_t *) "-->");
		UartSendString(SERIAL_PORT_PC, UartItoa(reg0[1],10));
		UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	}
	if(vsiz[i]>2)
	{
		UartSendString(SERIAL_PORT_PC, (uint8_t *) "-->");
		UartSendString(SERIAL_PORT_PC, UartItoa(reg0[2],10));
		UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	}
	if(vsiz[i]>3)
	{
		UartSendString(SERIAL_PORT_PC, (uint8_t *) "-->");
		UartSendString(SERIAL_PORT_PC, UartItoa(reg0[3],10));
		UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	}

	reg0[0]=0x00;
	reg0[1]=0x00;
	reg0[2]=0x00;
	reg0[3]=0x00;


	blinks();
	}

}

void dbn(long n)
{
    // array to store binary number
    int vecb[32];
    //vectores en binario de cada byte
    int newb0[8], newb1[8], newb2[8], newb3[8];

    // counter for binary array
    int i = 0;
    while (n > 0)
    {
        // storing remainder in binary array
        vecb[i] = n % 2;
        n = n / 2;
        i++;
    }
    for (int i = 0; i < 32; i++)
    {
        if (vecb[i] < 0) vecb[i] = 0;
    }

    for (int i = 0; i < 8; i++) newb3[i] = vecb[31 - i];
    for (int i = 0; i < 8; i++) newb2[i] = vecb[23 - i];
    for (int i = 0; i < 8; i++) newb1[i] = vecb[15 - i];
    for (int i = 0; i < 8; i++) newb0[i] = vecb[7 - i];

    //convertimos cada byte a decimal
    int decb3[8], decb2[8], decb1[8], decb0[8];
    int b3 = 0, b2 = 0, b1 = 0, b0 = 0;

    for (int i = 7; i > -1; i--)
    {
        if (newb3[i] == 1) decb3[7-i] = pot2(7-i);
        else decb3[7-i] = 0;
        if (newb2[i] == 1) decb2[7-i] = pot2(7-i);
        else decb2[7-i] = 0;
        if (newb1[i] == 1) decb1[7-i] = pot2(7-i);
        else decb1[7-i] = 0;
        if (newb0[i] == 1) decb0[7-i] = pot2(7-i);
        else decb0[7-i] = 0;
    }

    /*
    for (int i = 0; i <8 ; i++) printf("%d ", decb3[i]);
    for (int i = 7; i > -1; i--) printf("%d ", decb2[i]);
    for (int i = 7; i > -1; i--) printf("%d ", decb1[i]);
    for (int i = 7; i > -1; i--) printf("%d ", decb0[i]);
	*/

    for (int i = 0; i < 8; i++)
    {
        b3 = b3 + decb3[i];
        b2 = b2 + decb2[i];
        b1 = b1 + decb1[i];
        b0 = b0 + decb0[i];
    }

    /*
    printf("%d ", b3);
    printf("%d ", b2);
    printf("%d ", b1);
    printf("%d ", b0);
    */

    uint8_t ftwdef[5]={0x04, b3, b2, b1, b0};

	GPIOSetLow(GPIO_3);
	SpiWrite(SPI_1, ftwdef, 5);
	GPIOSetHigh(GPIO_3);
	toggleIOUP();

}


void read8302()
{
	//LECTURA DE GANANCIA Y FASE


	uint16_t readPH;
	uint16_t readDB;


	AnalogInputInit(&ADph_config);
	AnalogInputRead(CH2,&readPH);
	AnalogStartConvertion();

	AnalogInputInit(&ADdb_config);
	AnalogInputRead(CH1,&readDB);
	AnalogStartConvertion();

	//ENVIO DE LOS DATOS POR PUERTO SERIAL
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "G");
	UartSendString(SERIAL_PORT_PC, UartItoa(readPH,10));


	UartSendString(SERIAL_PORT_PC, (uint8_t *) "F");
	UartSendString(SERIAL_PORT_PC, UartItoa(readDB,10) );

	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");

}

void sweepftw(uint8_t numarm[],int cant, int rg, int stp)

{

	int pll= 384;
	//int pll= 96 ;
	//int pll=24;

	uint8_t pllword=0x87;
	//uint8_t pllword=0x27;
	//uint8_t pllword=0x07;

	configpll(pllword);

	double d32=4294967296;
	long ftword=0;

	float freqi=0;
	float freqf=0;

	double rang = (double) (rg/1000.0);
	double step= (double) (stp/1000000.0);
	//rang=0.125;
	//step=0.00005;

	int sweeplong=0;

	UartSendString(SERIAL_PORT_PC, (uint8_t *) "R");
	UartSendString(SERIAL_PORT_PC, UartItoa(rg,10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "S");
	UartSendString(SERIAL_PORT_PC, UartItoa(stp,10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");

	for (int n=0; n<cant; n++)
	{
		freqi= numarm[n] -rang;
		freqf= numarm[n] +rang;

		sweeplong= (freqf-freqi)/step;

		UartSendString(SERIAL_PORT_PC, (uint8_t *) "FQ");
		UartSendString(SERIAL_PORT_PC, UartItoa(numarm[n],10));
		UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r\n");

		for (int st=0; st<=sweeplong; st++)
		{
			ftword = (d32*freqi)/pll;
			dbn(ftword);

			read8302();

			freqi= freqi +step;
			ftword=0;
		}

	}

}

void oneftw(float freq)
{
	int pll= 384;
	//int pll= 96 ;
	//int pll=24;

	uint8_t pllword=0x87;
	//uint8_t pllword=0x27;
	//uint8_t pllword=0x07;

	configpll(pllword);

	long ftword=0;
	double d32=4294967296;

			ftword = (d32*freq)/pll;
			dbn(ftword);
}


void inis(void)
{
	SystemClockInit();
	Init_Leds();
	pins9951();

	UartInit(&usbserial);
	SpiInit(sintconf);

	config9951();


	Led_On(RGB_B_LED);
}

int main(void)
{
	inis();

/*
	timer_config tA = {TIMER_A,tsAD,&sweepftw};
	TimerInit(&tA);
	TimerStart(TIMER_A);
*/
	uint8_t vfs[3] = {10,30,50};

	sweepftw(vfs, 3, 150, 25);  //frecuencias-cant fqs-rango-step (FQS en Hz)
	//oneftw(10);  //10.2 clave

    while(1)
    {
    	//config9951();
    	//read9951();
    	//blinks2();
    	//toggleIOUP();
    	//read8302();
    	//oneftw(75);
    }

}

/*==================[end of file]============================================*/

