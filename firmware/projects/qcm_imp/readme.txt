proyecto_final	

El programa se comunica via ->SPI<- con el chip AD9951(slave) que se encuentra en un shield
Le enviaremos una instrucción en base al datasheet para que comience a funcionar
Seguidamente el shield empezará a funcionar y deberemos adquirir via ->ADC<-, los valores
de ganancia y fase que entrega el chip AD8302 del shield.
Por ultimo, enviaremos por puerto serial via ->UART<- estos valores para almacenarlos 
en un archivo de texto y finalmente ver gráficos e interpretar resultados
