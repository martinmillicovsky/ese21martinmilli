

/*==================[inclusions]=============================================*/
#include "ecg_pot.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"

static uint8_t ecgvs[] = {
17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
17,17,17
};

#define ts 10

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint16_t adv;
/*==================[internal functions declaration]=========================*/

void inis(void)
{
	SystemClockInit();
	LedsInit();

	serial_config usbserial = {SERIAL_PORT_PC, 9600, NULL};
	UartInit(&usbserial);

	analog_input_config AD_config = {CH1,AINPUTS_SINGLE_READ,NULL};
	AnalogInputInit(&AD_config);
}

void swr(void)
{
	AnalogInputReadPolling(CH1, &adv);
	float pot= ((((float)adv)/1023)*5);
	uint8_t vf=0;
	uint8_t* ecgv=&vf;

	for (int i=0; i<230; i++)
	{	vf=ecgvs[i]/pot;
		UartSendByte(SERIAL_PORT_PC, ecgv);
	}
}


void adcv (void)
{
	AnalogStartConvertion();
}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	inis();

	timer_config tA = {TIMER_A,ts,&swr};
	TimerInit(&tA);
	TimerStart(TIMER_A);

	/*
	timer_config tB = {TIMER_B,ts/2,&adcv};
	TimerInit(&tB);
	TimerStart(TIMER_B);
	*/
    while(1)
    {
    	LedOn(LED_3);
    	LedOn(LED_RGB_G);
    }

	return 0;
}

/*==================[end of file]============================================*/


